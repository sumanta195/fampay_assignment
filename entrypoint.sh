#!/bin/sh
echo "Waiting for mysql..."
while ! mysqladmin ping -h"db" --silent;
do
  sleep 1
done
echo "MySQL started"
python run.py