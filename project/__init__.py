import os
import time
from datetime import datetime as dt, timedelta, datetime
from flask import Flask, jsonify, request
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
import logging
from sqlalchemy.util.compat import contextmanager

from project.config import SQLALCHEMY_DATABASE_URI

app = Flask(__name__)

db = SQLAlchemy(session_options={'autocommit': False, 'autoflush': True})

def create_app():
    app.threaded = True
    app.processes = 5
    app.config['JSON_SORT_KEYS'] = False
    app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
    # enable CORS
    CORS(app)
    # set up extensions
    db.init_app(app)

    from project.api.ytms_api.ytms_view import ytms_api
    app.register_blueprint(ytms_api, url_prefix='/api')

    return app
# routes