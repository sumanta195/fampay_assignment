from flask import request, jsonify
from functools import wraps


def required_params(required):
    def decorator(fn):
        @wraps(fn)
        def wrapper(*args, **kwargs):
            _json = request.get_json()
            if _json is None:
                response = {
                    "status": "error",
                    "message": "This API Expects parameter. Passed None"
                }
                return jsonify(response), 400

            missing = [r for r in required.keys()
                       if r not in _json]

            if missing:
                response = {
                    "status": "error",
                    "message": f'Request data for {missing} is/are missing some required params',
                    "missing": {
                        "error": missing,
                        "type": "Required fields"
                    }
                }
                return jsonify(response), 400
            wrong_types = [r for r in required.keys()
                           if not isinstance(_json[r], required[r])]
            if wrong_types:
                response = {
                    "status": "error",
                    "message": f"Data types in the request data {wrong_types} doesn't match the required format",
                    "param_types": {
                        "error": wrong_types,
                        "type": "Data type mismatch"
                    }
                }
                return jsonify(response), 400
            return fn(*args, **kwargs)

        return wrapper

    return decorator