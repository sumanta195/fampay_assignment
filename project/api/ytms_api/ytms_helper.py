import datetime
import json
import traceback

from flask import request, jsonify
from sqlalchemy import text

from project import db
from project.config import yt_key
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

KEY = yt_key[0]
YOUTUBE_API_SERVICE_NAME = 'youtube'
YOUTUBE_API_VERSION = 'v3'


def getVideos():
    try:
        youtube = build('youtube', 'v3', developerKey=KEY)
        search_response = youtube.search().list(
            q=request.json['keyword'],
            type='video',
            part='id,snippet',
            maxResults=1000,
            order='relevance'
        ).execute()
        items = search_response['items']
        if not items:
            return {"status": "success", "message": "No YouTube results"}
        for i in range(len(items)):
            videoId = items[i]['id']['videoId']
            videoTitle = str(items[i]['snippet']['title']).replace("'", "")
            videoDescription = str(items[i]['snippet']['description']).replace("'", "")
            channelTitle = str(items[i]['snippet']['channelTitle']).replace("'", "")
            channelID = items[i]['snippet']['channelId']
            publishTime = items[i]['snippet']['publishedAt']
            fetchedTime = datetime.datetime.now()
            last_Updated = datetime.datetime.now()
            thumbnails = items[i]['snippet']['thumbnails']

            # check if already video id exist or not
            video_existing_checking = db.engine.execute(
                text(f"""select id from YTMS_VIDEO_DATA where videoId='{videoId}'""")).first()
            if video_existing_checking is None:
                # insert
                db.engine.execute(text(f"""insert into YTMS_VIDEO_DATA(videoId,videoTitle,videoDescription,channelTitle,channelID,publishTime,fetchedTime,last_Updated) 
                                                                values('{videoId}','{videoTitle}','{videoDescription}','{channelTitle}','{channelID}','{publishTime}','{fetchedTime}','{last_Updated}')"""))
                # add thumbnail to db
                default_th = thumbnails['default']
                db.engine.execute(text(f"""insert into YTMS_VIDEO_THUMBNAIL(videoID,thumbnail,thumbnail_type,height,width)
                    values('{videoId}','{default_th['url']}','default','{default_th['height']}','{default_th['width']}')"""))

        return {"status": "success", "message": "added to database"}
    except HttpError as he:
        return {"status": "error", "httpStatus": str(he.resp.status),
                "message": json.loads((he.content).decode('utf8'))}
    except Exception as e:
        return {"status": "error", "message": str(e)}


def getVideosAsync():
    try:
        print("*" * 10, "syncing started", "*" * 10)
        youtube = build('youtube', 'v3', developerKey=KEY)
        search_response = youtube.search().list(
            q='tseries',
            type='video',
            part='id,snippet',
            maxResults=1000,
            order='date'
        ).execute()
        items = search_response['items']
        if not items:
            print("*" * 10, "No Item from Youtube", "*" * 10)
            return {"status": "success", "message": "No YouTube results"}
        for i in range(len(items)):
            videoId = items[i]['id']['videoId']
            videoTitle = str(items[i]['snippet']['title']).replace("'", "")
            videoDescription = str(items[i]['snippet']['description']).replace("'", "")
            channelTitle = str(items[i]['snippet']['channelTitle']).replace("'", "")
            channelID = items[i]['snippet']['channelId']
            publishTime = items[i]['snippet']['publishedAt']
            fetchedTime = datetime.datetime.now()
            last_Updated = datetime.datetime.now()
            thumbnails = items[i]['snippet']['thumbnails']

            # check if already video id exist or not
            video_existing_checking = db.engine.execute(
                text(f"""select id from YTMS_VIDEO_DATA where videoId='{videoId}'""")).first()
            if video_existing_checking is None:
                print("*" * 10, "adding to database", "*" * 10)
                # insert
                db.engine.execute(text(f"""insert into YTMS_VIDEO_DATA(videoId,videoTitle,videoDescription,channelTitle,channelID,publishTime,fetchedTime,last_Updated) 
                                                                values('{videoId}','{videoTitle}','{videoDescription}','{channelTitle}','{channelID}','{publishTime}','{fetchedTime}','{last_Updated}')"""))
                # add thumbnail to db
                default_th = thumbnails['default']
                db.engine.execute(text(f"""insert into YTMS_VIDEO_THUMBNAIL(videoID,thumbnail,thumbnail_type,height,width)
                    values('{videoId}','{default_th['url']}','default','{default_th['height']}','{default_th['width']}')"""))
            print("*" * 10, "syncing completed", "*" * 10)
        return {"status": "success", "message": "syncing completed"}
    except Exception as e:
        return {"status": "error", "message": str(e)}


def doSearch():
    try:
        req = request.json
        keyWord = req['keyword']
        qry = db.engine.execute(text(f"""
            SELECT
                YTMS_VIDEO_DATA.videoId,
                YTMS_VIDEO_DATA.videoDescription,
                YTMS_VIDEO_DATA.publishTime,
                YTMS_VIDEO_DATA.fetchedTime,
                YTMS_VIDEO_THUMBNAIL.thumbnail,
                YTMS_VIDEO_THUMBNAIL.thumbnail_type,
                YTMS_VIDEO_DATA.videoTitle
            FROM
                YTMS_VIDEO_DATA
            JOIN YTMS_VIDEO_THUMBNAIL
            ON
                YTMS_VIDEO_THUMBNAIL.videoID = YTMS_VIDEO_DATA.videoId
            WHERE
                YTMS_VIDEO_DATA.videoTitle LIKE '%{keyWord}%' OR YTMS_VIDEO_DATA.videoDescription LIKE '%{keyWord}%' order by YTMS_VIDEO_DATA.publishTime desc
        """)).fetchall()
        resultSet = []
        for i in range(len(qry)):
            data = {
                "videoId": qry[i][0],
                "videoDescription": qry[i][1],
                "videoTitle": qry[i][6],
                "publishTime": qry[i][2],
                "fetchedTime": qry[i][3],
                "thumbnail": qry[i][4],
                "thumbnail_type": qry[i][5]
            }
            resultSet.append(data)
        return jsonify(resultSet)
    except Exception as e:
        return {"status": "error", "message": str(e)}


def getMyViodes():
    try:
        req = request.json
        lim = int(req['limit'])
        offset = int(req['offset'])
        qry = db.engine.execute(text(f"""
                SELECT
                    DISTINCT YTMS_VIDEO_THUMBNAIL.videoID,
                    YTMS_VIDEO_DATA.videoId,
                    YTMS_VIDEO_DATA.videoDescription,
                    YTMS_VIDEO_DATA.publishTime,
                    YTMS_VIDEO_DATA.fetchedTime,
                    YTMS_VIDEO_THUMBNAIL.thumbnail_type,
                    YTMS_VIDEO_THUMBNAIL.thumbnail,
                    YTMS_VIDEO_DATA.videoTitle
                FROM
                    YTMS_VIDEO_DATA
                JOIN YTMS_VIDEO_THUMBNAIL ON YTMS_VIDEO_THUMBNAIL.videoID = YTMS_VIDEO_DATA.videoId
                ORDER BY
                    YTMS_VIDEO_DATA.publishTime
                DESC
                LIMIT {offset},{lim}
            """)).fetchall()
        resultSet = []
        for i in range(len(qry)):
            data = {
                "videoId": qry[i][0],
                "videoTitle": qry[i][6],
                "videoDescription": qry[i][1],
                "publishTime": qry[i][2],
                "fetchedTime": qry[i][3],
                "thumbnail": qry[i][4],
                "thumbnail_type": qry[i][5]
            }
            resultSet.append(data)
        return jsonify(resultSet)
    except Exception as e:
        return {"status": "error", "message": str(e)}


def getAllVideos():
    try:
        qry = db.engine.execute(text(f"""SELECT
                            DISTINCT YTMS_VIDEO_THUMBNAIL.videoID,
                            YTMS_VIDEO_DATA.videoId,
                            YTMS_VIDEO_DATA.videoDescription,
                            YTMS_VIDEO_DATA.publishTime,
                            YTMS_VIDEO_DATA.fetchedTime,
                            YTMS_VIDEO_THUMBNAIL.thumbnail_type,
                            YTMS_VIDEO_THUMBNAIL.thumbnail,
                            YTMS_VIDEO_DATA.videoTitle
                        FROM
                            YTMS_VIDEO_DATA
                        JOIN YTMS_VIDEO_THUMBNAIL ON YTMS_VIDEO_THUMBNAIL.videoID = YTMS_VIDEO_DATA.videoId
                        ORDER BY
                            YTMS_VIDEO_DATA.publishTime
                        DESC
                    """)).fetchall()
        resultSet = []
        for i in range(len(qry)):
            data = {
                "videoId": qry[i][1],
                "videoTitle": qry[i][7],
                "videoDescription": qry[i][2],
                "publishTime": qry[i][3],
                "fetchedTime": qry[i][4],
                "thumbnail": qry[i][6],
                "thumbnail_type": qry[i][5]
            }
            resultSet.append(data)
        return {"status":"success", "message":"got data", "data":resultSet}
    except Exception as e:
        return {"status":"error", "message": str(e)}
