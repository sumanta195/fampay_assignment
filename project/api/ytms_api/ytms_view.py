from flask import Blueprint, request

from project.api.ytms_api.ytms_helper import getVideos, doSearch, getMyViodes, getAllVideos
from project.parser import required_params

ytms_api = Blueprint('ytms_api', __name__)


@ytms_api.route("/get/videos", methods=['POST'])
@required_params({'keyword': str})
def fetchYoutubeVideos():
    return getVideos()

@ytms_api.route("/search", methods=['POST'])
@required_params({'keyword': str})
def fetchSearch():
    return doSearch()


@ytms_api.route("/get/videos", methods=['GET'])
@required_params({'limit': str})
def getVideosFromDb():
    return getMyViodes()

@ytms_api.route("/all", methods=['GET'])
def getAll():
    return getAllVideos()