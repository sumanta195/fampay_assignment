CREATE DATABASE fampay_assignment;
use fampay_assignment;
CREATE TABLE IF NOT EXISTS `YTMS_KEYS` (
`slno` INT NOT NULL AUTO_INCREMENT,
`YTkey` varchar(255) NOT NULL,
`countLeft` varchar(255) NOT NULL,
PRIMARY KEY (`slno`)
);
CREATE TABLE IF NOT EXISTS `YTMS_VIDEO_DATA` (
`id` INT NOT NULL AUTO_INCREMENT,
`videoId` varchar(255) NOT NULL UNIQUE,
`videoTitle` TEXT NOT NULL,
`videoDescription` TEXT NOT NULL,
`channelTitle` varchar(255) NOT NULL,
`channelID` varchar(255) NOT NULL,
`publishTime` varchar(255) NOT NULL,
`fetchedTime` varchar(255) NOT NULL,
`last_Updated` varchar(255) NOT NULL,
PRIMARY KEY (`id`)
);
CREATE TABLE IF NOT EXISTS `YTMS_VIDEO_THUMBNAIL` (
`id` INT NOT NULL AUTO_INCREMENT,
`videoID` varchar(255) NOT NULL,
`thumbnail` TEXT NOT NULL,
`thumbnail_type` varchar(255) NOT NULL,
`height` varchar(255) NOT NULL,
`width` varchar(255) NOT NULL,
PRIMARY KEY (`id`)
);