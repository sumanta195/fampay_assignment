FROM python:3.9.0-slim

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

RUN apt-get -y update
RUN apt-get -y install mariadb-client

ADD . /usr/src/app
# add requirements (to leverage Docker cache)
ADD ./requirements.txt /usr/src/app/requirements.txt
# install requirements
RUN python -m pip install --upgrade pip

RUN pip install --no-cache-dir -r requirements.txt
RUN pip freeze > requirements.txt
# add entrypoint.sh
ADD ./entrypoint.sh /usr/src/app/entrypoint.sh
# execute permission to entrypoint
RUN chmod +x ./entrypoint.sh
RUN apt-get clean
# add app
EXPOSE 5000
# run server
CMD ["sh", "./entrypoint.sh"]