from flask_apscheduler import APScheduler
from project import create_app
from project.api.ytms_api.ytms_helper import getVideosAsync

app = create_app()
#
scheduler = APScheduler()


@scheduler.task('interval', id='do_job_1', seconds=60, misfire_grace_time=300)
def op():
    getVideosAsync()


scheduler.start()

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
