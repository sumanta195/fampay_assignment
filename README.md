# FamPay Assignment for Backend Developer.
> 
> This is a microservice architecture that has been designed to make the API call for the YouTube content.
```json
{
  "Author": "Sumanta Sharma",
  "Technology": "Flask (Python)",
  "Start Date": "04-09-2021"
}
```

### pre-requisites
> Docker

### How to start the application
> docker-compose -f docker-compose.yml up --build

### API Documentation
> Please visit (postman documentation) - https://documenter.getpostman.com/view/8017902/U16gP6zP
